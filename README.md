# gsi-vector-tiles-wb-style

国土地理院のベクトルタイルのスタイルを元にグレースケールの地図スタイルを作成するためのスクリプトです。

![サンプル](docs/sample.png)

## 処理内容

1. 元のスタイルを次からダウンロードする。
   https://github.com/gsi-cyberjapan/gsimaps-vector-experiment
1. ベクトル用のスタイルを読み込み、色設定の箇所をグレースケールに変更する。
1. スプライト画像をグレースケールに変更する。
 
### 変更対象のプロパティ
* layer.paint

`layer.paint`の以下のプロパティを読み込みグレースケールにする。

```
'line-color'
'fill-color'
'fill-outline-color'
'text-color'
'text-halo-color'
'line-gradient'
'icon-color'
'icon-halo-color'
```

* layer.metadata

`layer.metadata`以下のプロパティを読み込みグレースケールにする。

```
'fill-color'
```

## 使い方

### 設定

`make_style_to_gray.js`の以下の設定を変更する。
sprite_urlは相対URLを指定できないので、スプライトを置くURLを指定する。

```javascript
const sprite_url = 'https://mamemaki.gitlab.io/gsi-vector-tiles-wb-style/sprite';
````

### インストール

前提条件をインストールする。  
なぜかsharpがインストールできない場合があるので、その場合は以下のコマンドを実行する。

```bash
yarn install
# if you get an error about sharp, run this command
yarn add sharp --ignore-engines
```

### 白黒スタイルの作成

白黒地図のスタイルを作成するためのスクリプトを実行する。

```bash
bash create_gray_style.sh
```

### スタイルファイルの出力先

`style`ディレクトリにスタイルファイルが出力される。

## 出力サンプル

https://mamemaki.gitlab.io/gsi-vector-tiles-wb-style/

## FAQ
Q: アイコンの色が真っ黒なのでもう少し明るめにしたいです。

A: 今回使っているライブラリで調整できなかったので、スプライト画像を編集してください。