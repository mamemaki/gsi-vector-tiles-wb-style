const fs = require('fs');
const sharp = require('sharp');

// グレースケールスプライトのURL
const sprite_url = 'https://mamemaki.gitlab.io/gsi-vector-tiles-wb-style/style/sprite/std_gray';

// https://tannerhelland.com/2011/10/01/grayscale-image-algorithm-vb6.html
const red_gray_ratio = 0.2126;
const green_gray_ratio = 0.7152;
const blue_gray_ratio = 0.0722;
/*
const red_gray_ratio = 0.3;
const green_gray_ratio = 0.59;
const blue_gray_ratio = 0.11;
*/
const brightness_ratio = 2;

// RGB文字列をグレースケールに変換する
function toGrayscale(color) {
    let matches = color.match(/rgba?\((\d+),\s*(\d+),\s*(\d+),?\s*(\d*\.?\d*)?\)/i);
    if (matches) {
        let r = parseInt(matches[1]);
        let g = parseInt(matches[2]);
        let b = parseInt(matches[3]);
        let a = matches[4] ? parseFloat(matches[4]) : 1;
        let gray = Math.round(r * red_gray_ratio + g * green_gray_ratio + b * blue_gray_ratio);

        // 明るくする
        let hsv = rgb2hsv(gray, gray, gray);
        hsv.v += (100 - hsv.v) / brightness_ratio;
        let rgb = hsv2rgb(hsv.h, hsv.s, hsv.v);
        gray = Math.max(rgb.r, rgb.g, rgb.b);

        return `rgba(${gray},${gray},${gray},${a})`;
    }

    return color;
}

// RGBをHSVに変換する
// https://stackoverflow.com/questions/8022885/rgb-to-hsv-color-in-javascript
function rgb2hsv(r, g, b) {
    let rabs, gabs, babs, rr, gg, bb, h, s, v, diff, diffc, percentRoundFn;
    rabs = r / 255;
    gabs = g / 255;
    babs = b / 255;
    v = Math.max(rabs, gabs, babs),
        diff = v - Math.min(rabs, gabs, babs);
    diffc = c => (v - c) / 6 / diff + 1 / 2;
    percentRoundFn = num => Math.round(num * 100) / 100;

    if (diff === 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(rabs);
        gg = diffc(gabs);
        bb = diffc(babs);

        if (rabs === v) {
            h = bb - gg;
        } else if (gabs === v) {
            h = (1 / 3) + rr - bb;
        } else if (babs === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }
    return {
        h: Math.round(h * 360),
        s: percentRoundFn(s * 100),
        v: percentRoundFn(v * 100)
    };
}

// HSVをRGBに変換する
// https://stackoverflow.com/questions/17242144/javascript-convert-hsb-hsv-color-to-rgb-accurately/54024653#54024653
function hsv2rgb(h, s, v) {
    let r, g, b, i, f, p, q, t;
    h = h / 360;
    s = s / 100;
    v = v / 100;
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0:
            r = v, g = t, b = p;
            break;
        case 1:
            r = q, g = v, b = p;
            break;
        case 2:
            r = p, g = v, b = t;
            break;
        case 3:
            r = p, g = q, b = v;
            break;
        case 4:
            r = t, g = p, b = v;
            break;
        case 5:
            r = v, g = p, b = q;
            break;
    }

    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255),
    };

}

// JSONデータ内の各レイヤーの色をグレースケールに変換
function makeStyleToGray(style) {
    let grayStyle = {...style};
    if (grayStyle && grayStyle.layers) {
        grayStyle.layers.forEach(layer => {
            if (layer.paint) {
                if (layer.paint['line-color']) {
                    layer.paint['line-color'] = toGrayscale(layer.paint['line-color']);
                }
                if (layer.paint['fill-color']) {
                    layer.paint['fill-color'] = toGrayscale(layer.paint['fill-color']);
                }
                if (layer.paint['fill-outline-color']) {
                    layer.paint['fill-outline-color'] = toGrayscale(layer.paint['fill-outline-color']);
                }
                if (layer.paint['text-color']) {
                    layer.paint['text-color'] = toGrayscale(layer.paint['text-color']);
                }
                if (layer.paint['text-halo-color']) {
                    layer.paint['text-halo-color'] = toGrayscale(layer.paint['text-halo-color']);
                }
                if (layer.paint['line-gradient']) {
                    layer.paint['line-gradient'] = toGrayscale(layer.paint['line-gradient']);
                }
                if (layer.paint['icon-color']) {
                    layer.paint['icon-color'] = toGrayscale(layer.paint['icon-color']);
                }
                if (layer.paint['icon-halo-color']) {
                    layer.paint['icon-halo-color'] = toGrayscale(layer.paint['icon-halo-color']);
                }

            }
            if (layer.metadata) {
                if (layer.metadata['fill-color']) {
                    layer.metadata['fill-color'] = toGrayscale(layer.metadata['fill-color']);
                }
            }
        });
    }

    if (grayStyle && grayStyle.sprite) {
        grayStyle.sprite = sprite_url;
    }

    return grayStyle;
}

const style = JSON.parse(fs.readFileSync('style/std.json', 'utf8'));
const grayStyle = makeStyleToGray(style);
fs.writeFileSync('style/std_gray.json', JSON.stringify(grayStyle, null, 4));

// 画像をグレースケールに変換
sharp('style/sprite/std.png')
    .grayscale()
    .modulate({brightness: 2})
    .toFile('style/sprite/std_gray.png');

// スプライト設定をコピー
fs.copyFile('style/sprite/std.json', 'style/sprite/std_gray.json', (err) => {
    if (err) throw err;
});