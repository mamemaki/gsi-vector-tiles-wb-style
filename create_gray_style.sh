#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)
STYLE_DIR=${SCRIPT_DIR}/style
SPRITE_DIR=${SCRIPT_DIR}/style/sprite

mkdir -p "$STYLE_DIR"
mkdir -p "$SPRITE_DIR"

# 元スタイルファイルを取得
wget -O "$STYLE_DIR"/std.json https://gsi-cyberjapan.github.io/gsivectortile-mapbox-gl-js/std.json
wget -O "$SPRITE_DIR"/std.json https://gsi-cyberjapan.github.io/gsivectortile-mapbox-gl-js/sprite/std.json
wget -O "$SPRITE_DIR"/std.png https://gsi-cyberjapan.github.io/gsivectortile-mapbox-gl-js/sprite/std.png

# スタイルファイルをグレースケールに変換
node make_style_to_gray.js

# 元スタイルファイルを削除
rm "$STYLE_DIR"/std.json
rm "$SPRITE_DIR"/std.json
rm "$SPRITE_DIR"/std.png

# jsonファイルのフォーマットを整える


# 作成したスタイルファイルをサンプルディレクトリにコピー
mkdir -p public/style/sprite
jq -c . < "$STYLE_DIR"/std_gray.json > public/style/std_gray.json
jq -c . < "$SPRITE_DIR"/std_gray.json > public/style/sprite/std_gray.json
cp "$SPRITE_DIR"/std_gray.png public/style/sprite/std_gray.png
